"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const userSchema = joi_1.default.object({
    username: joi_1.default.string().required,
    password: joi_1.default.string().min(6).required
});
const validateUser = (req, res, next) => {
    const { error } = userSchema.validate(req.body);
    if (error) {
        res.status(400).send(error.details[0].message);
    }
    else {
        next();
    }
};
exports.default = validateUser;
