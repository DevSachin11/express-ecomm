"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userRoutes_1 = __importDefault(require("./routes/userRoutes"));
const LoggerMiddleware_1 = __importDefault(require("./middleware/LoggerMiddleware"));
const ValidateUser_1 = __importDefault(require("./middleware/ValidateUser"));
const app = (0, express_1.default)();
app.use(LoggerMiddleware_1.default);
app.use('/api/users', ValidateUser_1.default, userRoutes_1.default);
const PORT = 3000;
// app.get('/', (req: Request, res: Response) => {
//     res.send("Inside TS app")
// });
//app.use(express.json())
app.use('/users', userRoutes_1.default);
app.listen(PORT, () => {
    console.log(` Server running on http://localhost:${PORT}`);
});
