import { Request, Response, NextFunction } from "express";

import Joi from 'joi';

const userSchema = Joi.object({
    username: Joi.string().required,
    password: Joi.string().min(6).required
})

const validateUser = (req: Request, res:Response, next: NextFunction): void => {
    const {error} = userSchema.validate(req.body)
    if(error){
        res.status(400).send(error.details[0].message)
    }
    else{
        next()
    }
}

export default validateUser