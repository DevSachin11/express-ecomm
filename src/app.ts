import express from 'express';
import userRouter from './routes/userRoutes'
import loggerMiddleware from './middleware/LoggerMiddleware'
import validateUser from './middleware/ValidateUser';

const app = express()
app.use(loggerMiddleware)
app.use('/api/users',validateUser, userRouter)
const PORT = 3000

// app.get('/', (req: Request, res: Response) => {
//     res.send("Inside TS app")
// });

//app.use(express.json())

app.use('/users', userRouter)

app.listen(PORT, () => {
    console.log(` Server running on http://localhost:${PORT}`)
})